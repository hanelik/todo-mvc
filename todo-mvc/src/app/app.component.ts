import { Component } from "@angular/core";
import { StoreService } from "./store.service";
import { Todo } from "./model/todo";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "Ondrej todos";
  newValue: string = "";

  todos: Array<Todo> = [];

  constructor(private store: StoreService) {}

  add() {
    if (this.newValue) {
      this.store.add(this.newValue);
      this.todos = this.store.todos;
      this.newValue = "";
    }
  }

  delete(todo: Todo) {
    this.store.delete(todo.id);
    this.todos = this.store.todos;
  }

  toggle(todo: Todo) {
    this.store.toggle(todo.id);
    this.todos = this.store.todos;
  }
}
