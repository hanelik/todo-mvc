export class Todo {
  text: string;
  id: number;
  finished: boolean;

  constructor(text: string) {
    this.text = text;
    this.id = new Date().getTime();
  }
}
