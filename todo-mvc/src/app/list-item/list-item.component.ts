import { Component, OnInit, Input, Output } from "@angular/core";
import { Todo } from "../model/todo";
import { EventEmitter } from "@angular/core";

@Component({
  selector: "app-list-item",
  templateUrl: "./list-item.component.html",
  styleUrls: ["./list-item.component.scss"]
})
export class ListItemComponent implements OnInit {
  @Input() todo: Todo;

  @Output() toggled: EventEmitter<Todo> = new EventEmitter();

  @Output() deleted: EventEmitter<Todo> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  delete(): void {
    this.deleted.emit(this.todo);
  }

  toggle(): void {
    this.toggled.emit(this.todo);
  }
}
