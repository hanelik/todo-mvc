import { Injectable } from "@angular/core";
import { Todo } from "./model/todo";

@Injectable({
  providedIn: "root"
})
export class StoreService {
  todos: Array<Todo> = [];

  constructor() {}

  add(text: string) {
    this.todos = [...this.todos, new Todo(text)];
  }

  toggle(id: number) {
    const todo = this.getById(id);
    todo.finished = !todo.finished;
  }

  delete(id: number) {
    this.todos = [...this.todos.filter(item => item.id !== id)];
  }

  private getById(id: number): Todo {
    return this.todos.find(todo => todo.id === id);
  }
}
